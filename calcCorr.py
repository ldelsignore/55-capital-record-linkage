
import numpy as np
import openpyxl
import sys
import csv
import pickle

def fillMat(totalReturnData, corrMat):
	for i in range(size):
		for j in range(size):
			if len(totalReturnData[i]) == len(totalReturnData[j]) and totalReturnData[i] != []:
					corrMat[i][j] = np.corrcoef(totalReturnData[i], totalReturnData[j])[0][1]
			else:
				 corrMat[i][j] = 0
	return corrMat


if __name__ == '__main__' :

	totalReturnData = pickle.load(open("returnData.p", "rb"))

	size = len(totalReturnData)
	corrMat = [[0.0 for x in range(size)] for x in range(size)]

	corrMat = fillMat(totalReturnData, corrMat)			

	with open(sys.argv[1], 'wb') as fileName:
		pickle.dump(corrMat, fileName)		

