
# Luca Del Signore
# 55 Capital Partners

# This program contains the main functionality of the
# code base. It has classes that represent different
# types of attributes comparable between securities.
# Their objects are used to compare all attributes
# of one type belonging to one record with all attributes
# of the same type belonging to the other records.
# As long as two records are considered "sufficiently"
# simmilar, they will be added to a list of probable
# duplicates of that record.


import math
import Levenshtein as LV
import csv
import openpyxl
import sys
import pickle

class ETFattribute(object):

	# Initializes the Levenshtein matrix and the array of
	# attribute data
	# @param size: the number of ETFs being analyzed
	# @param columnNumber: the column number of the current
	# 		 			   attribute
	def __init__(self, columnNumber):
		wb = openpyxl.load_workbook(sys.argv[1])		
		wb.get_sheet_names()
		sheet = wb.active
		self.size = sheet.max_row - 1
		self.records = []
		for i in range(self.size):
			self.records.append(sheet.cell(row=i+2,column=columnNumber).value)
		self.mat = [[0.0 for x in range(self.size)] for x in range(self.size)]
		self.fillMat()

	# Fills the matrix data structure with Levenshtein
	# distances
	def fillMat(self):
		for i in range(len(self.records)):
			for j in range(len(self.records)):
				self.mat[i][j] = LV.distance(self.records[i], self.records[j])

	# Scan through Levenshtein distance matrix and filter
	# out all pairs that are less than the distance 
	# threshold	
	def filterDistancesAndApply(self, ETFnames, ETFgroup, i):
		for j in range(ETFnames.size):
			threshold = self.setThreshold(i, j)
			if self.mat[i][j] <= threshold and i != j:
				addETF(j, ETFnames.records, ETFgroup)

class homogenousAttribute(ETFattribute):

	# Because the attributes of this type are fairly 
	# simmilar to each other, they will tend to have 
	# a low Levenshtein distance. This function starts
	# the distance at zero and raises it if the average
	# length of the strings being compared is high
	def setThreshold(self, i, j):
		avgLen = math.floor((len(self.records[i]) + len(self.records[j])) / 2)
		if len(self.records[i]) <= 3 and len(self.records[j]) <= 3:
			threshold = 0
		else:
			if avgLen <= 4:
				threshold = 0
			else:
				if avgLen > 4 and avgLen <= 8:
					threshold = 1
				elif avgLen > 8 and avgLen <= 10:
					threshold = 2
				else:
					threshold = 3
		return threshold
		
class inhomogenousAttribute(ETFattribute):

	# The attributes of this type are less homogenous,
	# so they automatically get a Levenshtein distance
	# of three unlike the homogenous ones, which normally
	# get zero. I might add in something about longer
	# strings getting largers distances, but that does
	# not seem to be neccesary right now.
	def setThreshold(self, i, j):	
		threshold = 3
		threshold += int(sys.argv[3]) # add scaling factor
		if threshold < 0:
			threshold = 0
		return threshold

class correlationData(object):

	# Load the correlation data matrix from outside
	# the program and set the correlation threshold
	def __init__(self):
		self.correlationMat = pickle.load(open(sys.argv[4], "rb"))
		self.threshold = self.setCorrThreshold()

	# Scan through the correlation data matrix and
	# filter out all pairs that are less than the 
	# correlation threshold
	def filterCorrelationsAndApply(self, ETFnames, ETFgroup, i):
		for j in range(ETFnames.size):
			if self.correlationMat[i][j] >= self.threshold and i != j:
				addETF(j, ETFnames.records, ETFgroup)

	# Set correlation threshold
	def setCorrThreshold(self):
		return float(sys.argv[5])

# add an ETF to the list of related ETFs
# @param j: Index used to select from list
# of ETFs
def addETF(j, ETFrecords, ETFgroup):
	if ETFrecords[j] not in ETFgroup[0]:
		ETFgroup[0].append(ETFrecords[j])

# remove an ETF to the list of related ETFs
# @param j: Index used to select from list
# of ETFs
def removeETF(j, ETFrecords, ETFgroup):
	if ETFrecords[j] in ETFgroup[0]:
		ETFgroup[0].remove(ETFrecords[j])

# For each ETF, calls all of the functions that find probable
# duplicates of that ETF using both Levenshtien distance data
# and correlation data.
def writeCSV(indexNames, indexTickers, tickerNames, ETFnames, correlations):	
	with open(sys.argv[2], 'w') as csvfile:
		fieldnames = ['ETFticker', 'aggreagteRelatedETFs']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()
		rowCommand = []
		for i in range(ETFnames.size):
			ETFgroup = []
			ETFgroup.append([])
			rowCommand.append(('ETFticker', ETFnames.records[i]))
			indexNames.filterDistancesAndApply(ETFnames, ETFgroup, i)
			indexTickers.filterDistancesAndApply(ETFnames, ETFgroup, i)
			tickerNames.filterDistancesAndApply(ETFnames, ETFgroup, i)
			correlations.filterCorrelationsAndApply(ETFnames, ETFgroup, i)
			rowCommand.append(('aggreagteRelatedETFs', ETFgroup[0]))
			writer.writerow(dict(rowCommand))

# @param argv[1]: name of input ETF file in xlsx format
# @param argv[2]: name of output file in csv format
# @param argv[3]: Levenshtien scaling factor
# @param argv[4]: name of correlation data file in pickle
# 				  format
# @param argv[5]: Correlation scaling factor
if __name__ == '__main__' :

	correlations = correlationData()

	# Initialize attribute objects with column numbers
	indexNames = inhomogenousAttribute(4)
	indexTickers = homogenousAttribute(3)
	tickerNames = inhomogenousAttribute(2)
	ETFnames = ETFattribute(1) # not used in the same way as other attributes
							   # in this version of the program, so superclass
							   # is used here. It can be changed later.	
		
	writeCSV(indexNames, indexTickers, tickerNames, ETFnames, correlations)

