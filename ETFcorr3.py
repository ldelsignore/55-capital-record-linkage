
import openpyxl
import csv
import sys

if __name__ == '__main__' :

	xlsxFile = sys.argv[1]
	oldCsvFile = sys.argv[2]
	newCsvFile = sys.argv[3]

	wb = openpyxl.load_workbook(xlsxFile)
	sheet = wb.active
	size = sheet.max_row - 1

	# Write header
	with open(newCsvFile, 'a') as newCSV:
		fieldnames = ['month', 'tick', 'return']
		writer = csv.DictWriter(newCSV, fieldnames=fieldnames)
		writer.writeheader()

	# Write all following rows 
	with open(oldCsvFile) as oldCSV:
		reader = csv.DictReader(oldCSV)
		for row in reader:
			for i in range(size):
				currETF = sheet.cell(row=i+2, column=1).value
				if row['tick'] == currETF:
					month = row['monthnumber']
					tick = row['tick']
					Return = row['return']
					with open(newCsvFile, 'a') as newCSV:
						fieldnames = ['month', 'tick', 'return']
						writer = csv.DictWriter(newCSV, fieldnames=fieldnames)
						writer.writerow({'month' : month, 'tick' : tick, 'return' : Return})


