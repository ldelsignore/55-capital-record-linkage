
from Tkinter import *
from subprocess import call

root = Tk()
root.wm_title("Duplicate Finder")
root.minsize(250, 200)
root.maxsize(300, 350)

frame1 = Frame(root); frame1.pack()
frame2 = Frame(frame1); frame2.pack(side=BOTTOM)
frame3 = Frame(frame2); frame3.pack(side=BOTTOM)


def createFile():
	xlsxFilePath = entry1.get()
	oldCsvFilePath = entry2.get()
	finalCorrFilePath = entry3.get()
	print "testA"
	call(["python", "ETFcorr3.py", xlsxFilePath, oldCsvFilePath, "interim.csv"])	
	print "testB"
	call(["python", "findCorr3.py", xlsxFilePath, "interim.csv"])
	print "testC"
	call(["python", "calcCorr.py", finalCorrFilePath])
	print "testD"



label1 = Label(frame1, text="name/path of xlsx file")
label1.pack(side=TOP)
entry1 = Entry(frame1, bd=5)
entry1.pack()


label2 = Label(frame2, text="name/path of csv file")
label2.pack(side=TOP)
entry2 = Entry(frame2, bd=5)
entry2.pack()

label3 = Label(frame3, text="name/path of output file")
label3.pack(side=TOP)
entry3 = Entry(frame3, bd=5)
entry3.pack()

button = Button(frame3, text="Create correlation data file", command=createFile)
button.pack(side=BOTTOM)

root.mainloop()
