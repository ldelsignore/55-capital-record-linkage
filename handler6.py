
# This handler program provides a GUI interface for 
# simplified use of the Levmap program. It gathers
# data from the user and then passes them as arguements
# to levMap.

from Tkinter import *
from subprocess import call

root = Tk()
root.wm_title("Duplicate Finder")
root.minsize(250, 200)
root.maxsize(300, 350)

frame1 = Frame(root); frame1.pack(side=TOP)
frame2 = Frame(frame1); frame2.pack(side=TOP)
frame3 = Frame(frame2); frame3.pack(side=TOP)
frame4 = Frame(frame3); frame4.pack(side=TOP)
frame_sub1 = Frame(frame1); frame_sub1.pack(side=BOTTOM)
frame_sub2 = Frame(frame_sub1); frame_sub2.pack(side=BOTTOM)

# Passes parameters to the duplicate finder
def runLevMap():
	inputFilePath = entry1.get()
	outputFilePath = entry2.get()
	corrFilePath = entry3.get()
	LVscalingFactor = str(scale.get())
	corrScalingFactor = entry4.get()
	#print inputFilePath, outputFilePath, corrFilePath, LVscalingFactor, corrScalingFactor
	call(["python", "levMap12.py", inputFilePath, outputFilePath, LVscalingFactor, corrFilePath, corrScalingFactor])

label1 = Label(frame4, text="name/path of ETF file")
label1.pack(side=TOP)

entry1 = Entry(frame4, bd=5)
entry1.pack()

label2 = Label(frame4, text="name/path of output file")
label2.pack(side=BOTTOM)

entry2 = Entry(frame3, bd=5)
entry2.pack()

label5 = Label(frame2, text="name/path of correlation file")
label5.pack()

entry3 = Entry(frame2, bd=5)
entry3.pack(side=BOTTOM)

label3 = Label(frame_sub1, text="Correlation threshold")
label3.pack(side=TOP)

defaultCorrelation = DoubleVar()
defaultCorrelation.set(0.99) # Set the default correlation threshold
entry4 = Entry(frame_sub1, bd=5, width=5, textvariable=defaultCorrelation)
entry4.pack()

label4 = Label(frame_sub2, text="Levenshtein scaling factor")
label4.pack(side=TOP)

var = IntVar()
scale = Scale(frame_sub2, variable=var, orient=HORIZONTAL, from_=-3, to=3)
scale.pack()

button = Button(frame_sub2, text="Create output file", command=runLevMap)
button.pack(side=BOTTOM)

root.mainloop()

