
# This program grabs the monthly return data on
# various ETFs from a csv file and stores them 
# in a pickle file for later use.

import openpyxl
import sys
import csv
import pickle

class Singleton:

	def __init__(self, source, currETF):
		self.reader = csv.DictReader(source)
		self.START_MONTH = 404
		self.END_MONTH = 440 # the input file should have
							 # no return for END_MONTH
		self.startFlag = False
		self.endFlag = False
		self.currETF = currETF		
		self.dataArray = []

	# As getReturn data scans through the ETF records, the 
	# following two functions determine whether the current
	# record being examined corresponds to the desired ETF

	def checkForStart(self, row):
		if int(row['month']) == self.START_MONTH:
			if row['tick'] == self.currETF:
				self.startFlag = True; self.endFlag = False
	
	def checkForEnd(self, row):
		if int(row['month']) == self.END_MONTH:	
			if row['tick'] == self.currETF:
				self.startFlag = False; self.endFlag = True

	# Returns a 1-dimensional array
	# of return data for the currETF
	def getReturnData(self):
		
		for row in self.reader:
			self.checkForStart(row)
			self.checkForEnd(row)
			if self.startFlag == True:
				if self.checkForExceptions(row) == True:
					return []
				if self.currETF == row['tick']:
					self.dataArray.append(float(row['return']))
			elif self.endFlag == True:
				return self.dataArray			
		
	# Checks for empty entries that cannot be processed into 
	# correlation data
	def checkForExceptions(self, row):
		if row['tick'] == "" or row['return'] == "" or row['month'] == "":
			return True
		else:
			return False


if __name__ == '__main__' :


	wb = openpyxl.load_workbook(sys.argv[1])
	sheet = wb.active
	size = sheet.max_row - 1

	totalReturnData = []

	for i in range(size):		
		with open(sys.argv[2]) as source:
			currETF = sheet.cell(row=i+2, column=1).value
			singleton = Singleton(source, currETF)
			totalReturnData.append(singleton.getReturnData())

	with open('returnData.p', 'wb') as fileName:
		pickle.dump(totalReturnData, fileName)

